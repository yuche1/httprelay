module gitlab.com/jonas.jasas/httprelay

go 1.19

require (
	gitlab.com/jonas.jasas/buffreader v0.0.0-20200406102452-cad14a5681d3
	gitlab.com/jonas.jasas/bufftee v0.0.0-20190309090616-0f18f95bd4d2
	gitlab.com/jonas.jasas/closechan v0.0.0-20200419060521-9f8eba9e316d
	gitlab.com/jonas.jasas/rwmock v0.0.0-20190210172938-6ed2091800f6
)
